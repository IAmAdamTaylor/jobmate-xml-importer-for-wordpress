<?php

/**
 * Responsible for processing a single job and adding it to the WordPress database.
 *
 * @link       http://www.iamadamtaylor.com
 * @since      1.0.0
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 */

/**
 * Responsible for processing a single job and adding it to the WordPress database.
 *
 * Parses the maeta from the XML feed into usable values.
 * Creates job posts based on the parsed values
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 * @author     Adam Taylor <sayhi@iamadamtaylor.com>
 */
class JobMate_XML_Job {

	/**
	 * The unique ID for this job.
	 * @var integer
	 */
	public $ID;

	/**
	 * The status of the current import.
	 * Can be:
	 * 	processing
	 * 	created
	 * 	updated
	 * 	deleted
	 * @var string
	 */
	public $status;

	/**
	 * The XML node being processed.
	 * @var SimpleXMLElement
	 */
	protected $_xml;

	/**
	 * The post data for this XML node. Only available once parsed.
	 * @var array
	 */
	protected $_post = array();

	/**
	 * The post meta for this XML node. Only available once parsed.
	 * @var array
	 */
	protected $_meta = array();

	/**
	 * Logger for writing debug statements
	 * @var JobMate_XML_Importer_Log
	 */
	protected $_log;
	
	/**
	 * Parse the XML node on init
	 * @param SimpleXMLElement $xml The XML node to parse. Must be a <job> node.
	 */
	function __construct( SimpleXMLElement $xml ) {
		if ( 'job' !== $xml->getName() ) {
			$exception_message = "XML Node passed must be a <job> node";
			$this->_log->write( "Thrown Exception: " . $exception_message );
			throw new JobMate_XML_Job_Exception( $exception_message );
		}

		// Init logger
		$this->_log = JobMate_XML_Importer_Log::get_instance();

		$this->_xml = $xml;
		$this->_log->write( 'XML Request started. Request is:' );
		$this->_log->write( (string)$xml->asXML(), true );

		// Set status to processing
		$this->status = 'processing';

		$this->_parse();

		$this->_log->write( 'Request has been processed with no fatal errors' );		
		$this->_log->write( 'Request status was: ' . $this->status );		
	}

	/**
	 * Parse the XML node property.
	 */
	protected function _parse() {
		$this->ID = intVal( $this->_xml->uniqueid );
		$this->_meta[ 'unique_id' ] = $this->ID;

		$this->_log->write( 'Beginning to parse. Job\'s Unique ID is: ' . $this->ID );

		$post_id = $this->_get_post_id( $this->ID );
		$this->_log->write( 'Post ID is: ' . ( ( false === $post_id ) ? 'false' : $post_id ) );
		if ( false !== $post_id ) {
			$this->_log->write( 'Post already exists within the WordPress database' );
		} else {
			$this->_log->write( 'Post does not exist within the WordPress database' );
		}

		// Check for a remove (delete) tag
		if ( isset( $this->_xml->remove ) ) {
			// To be able to delete a post, it must also have a post ID
			if ( false !== $post_id ) {
				$this->_delete_post( $post_id );
			}
			return;
		}

		// Parse the post object nodes
		$this->_post = array(
			'post_content' => trim( $this->_sanitise_post_content( (string)$this->_xml->jobdescription ) ),
			'post_excerpt' => trim( $this->_sanitise_post_content( (string)$this->_xml->jobsummary ) ),
			'post_title' => trim( (string)$this->_xml->jobtitle ),
			'post_status' => 'publish',
			'post_type' => 'job'
		);

		// Parse simple string nodes
		$meta_string_nodes = array(
			'lastmodified' => 'last_modified',
			'jobreference' => 'job_reference',
			'hours' => 'hours',
			'jobtype' => 'job_type',
			'jobduration' => 'job_duration',
			'branchname' => 'branch_name',
			'companyname' => 'company_name',
			'immediatestart' => 'immediate_start',
			'application_email' => 'apply_email',
			'URL' => 'apply_url'
		);

		foreach ($meta_string_nodes as $node_name => $parsed_key ) {
			$this->_meta[ $parsed_key ] = (string)$this->_xml->{ $node_name };
		}

		// Parse keyed arrays

			// Parse location name
			$this->_meta[ 'location' ] = (string)$this->_xml->location->name;
			
			// Parse the latitude & longitude
			$this->_meta[ 'latitude' ] = (string)round( (string)$this->_xml->location->latitude, 5 );
			$this->_meta[ 'longitude' ] = (string)round( (string)$this->_xml->location->longitude, 5 );

			// Parse date range
			$this->_meta[ 'start_date' ] = (integer)$this->_xml->daterange->startdate;
			$this->_meta[ 'end_date' ] = (integer)$this->_xml->daterange->enddate;

			// Parse salary
			$this->_meta[ 'salary' ] = array(
				'min' => (integer)$this->_xml->salary->min,
				'max' => (integer)$this->_xml->salary->max,
				'type' => (string)$this->_xml->salary->type
			);

			// Add the min salary as an admin only meta key to enable sorting
			$this->_meta[ '_salary_sort' ] = (integer)$this->_xml->salary->min;

		// Parse 1 dimensional arrays

			// Parse key features
			$child_nodes = $this->_xml->keyfeatures->children();
			$this->_meta[ 'key_features' ] = array();
			foreach ($child_nodes as $child_node) {
				$this->_meta[ 'key_features' ][] = (string)$child_node;
			}

			// Parse benefits
			$child_nodes = $this->_xml->benefits->children();
			$this->_meta[ 'benefits' ] = array();
			foreach ($child_nodes as $child_node) {
				$this->_meta[ 'benefits' ][] = (string)$child_node;
			}

			// Parse job requirements
			$child_nodes = $this->_xml->jobrequirements->children();
			$this->_meta[ 'job_requirements' ] = array();
			foreach ($child_nodes as $child_node) {
				$this->_meta[ 'job_requirements' ][] = (string)$child_node;
			}

		// Check if post already exists
			// Create it if it doesn't
			// Check if it needs to be updated if it does
		if ( false === $post_id ) {
			
			$post_id = $this->_create_post();
			
		} else {

			if ( $this->_should_post_be_updated( $post_id ) ) {
				$this->_post['ID'] = $post_id;
				$post_id = $this->_update_post();
			} else {
				$this->_log->write( 'Post object is already up to date' );
				$this->status = 'processed';
			}

		}

		// Parse the sector as a taxonomy term
		$taxonomy_name = 'sector';
		$sector_name = (string)$this->_xml->sector;
		$taxonomy_manager = new JobMate_XML_Importer_Taxonomy_Manager( $taxonomy_name );
		$sector_term = $taxonomy_manager->get_term( $sector_name );

		// Only set the term if it doesn't already exist on the post
		if ( !$this->_post_has_term( $post_id, $sector_term, $taxonomy_name ) ) {
			$this->_set_post_terms( $post_id, $sector_term->term_id, $taxonomy_name );

			// Update status
			if ( 'created' !== $this->status ) {
				$this->status = 'updated';
			}
		} else {
			$this->_log->write( "Post ID $post_id terms for taxonomy $taxonomy_name are already set correctly." );
		}

	}

	/**
	 * Sanitise the string passed, so that it can be used as post content.
	 * @param  string $content The content to sanitise.
	 * @return string          The sanitised content.
	 */
	protected function _sanitise_post_content( $content ) {
		// Combine multiple <br> tags
		$content = preg_replace( '/(?:<br\s?\/?>)+/', '<br>', $content );

		// Remove VB code
		$content = preg_replace( '/\"\s*&\s*vbCrLf\s*&\s*\"/', '', $content );

		return $content;
	}

	/**
	 * Create a new WordPress job post using the parsed details.
	 * @return integer The post ID of the created post.
	 */
	protected function _create_post() {
		// Check node has been parsed
		if ( !isset( $this->_post ) || !isset( $this->_meta ) ) {
			return;
		}

		$this->_log->write( 'Creating new post. post.json is:' );
		$this->_log->write( "\t" . json_encode( $this->_post ), true );
		$post_id = wp_insert_post( $this->_post, true );

		if ( 0 === $post_id || is_wp_error( $post_id ) ) {
			$exception_message = "Job {$this->ID} could not be created by WordPress";
			$this->_log->write( "Thrown Exception: " . $exception_message );
			throw new JobMate_XML_Job_Exception( $exception_message );
		}

		$this->_log->write( 'Created successfully with ID: ' . $post_id );
		$this->_update_meta( $post_id );

		// Update status
		$this->status = 'created';

		return $post_id;
	}

	/**
	 * Update an existing WordPress job post using the parsed details.
	 * @return integer The post ID of the updated post.
	 */
	protected function _update_post() {
		// Check node has been parsed
		if ( !isset( $this->_post ) || !isset( $this->_meta ) ) {
			return;
		}

		$this->_log->write( 'Updating post with ID: ' . $this->_post['ID'] . '. post.json is:' );
		$this->_log->write( "\t" . json_encode( $this->_post ), true );
		$post_id = wp_update_post( $this->_post, true );

		if ( 0 === $post_id || is_wp_error( $post_id ) ) {
			$exception_message = "Job {$this->ID}, ID #{$post_id} could not be updated by WordPress";
			$this->_log->write( "Thrown Exception: " . $exception_message );
			throw new JobMate_XML_Job_Exception( $exception_message );
		}

		$this->_update_meta( $post_id );

		// Update status
		$this->status = 'updated';

		return $post_id;
	}

	/**
	 * Delete an existing WordPress job post.
	 * @param integer $post_id The post ID to delete.
	 */
	public function _delete_post( $post_id ) {
		$this->_log->write( 'Deleting post with ID: ' . $post_id );
		wp_delete_post( $post_id );

		// Update status
		$this->status = 'deleted';
	}

	/**
	 * Update the post meta for the newly created or existing post.
	 * @param  integer $post_id The post ID to update.
	 * @return integer The post ID of the updated post.
	 */
	protected function _update_meta( $post_id ) {
		$this->_log->write( 'Updating post meta for ID: ' . $post_id . '. meta.json is:' );
		$this->_log->write( "\t" . json_encode( $this->_meta ), true );

		foreach ($this->_meta as $key => $value) {
			if ( is_string( $value ) ) {
				$value = trim( $value );
			}
			update_post_meta( $post_id, $key, $value );
		}

		return $post_id;
	}

	/**
	 * Set the taxonomy terms on the post.
	 * @param integer $post_id The post ID to update.
	 * @param integer $term_id The term ID to set.
	 * @param string $taxonomy The taxonomy name.
	 */
	protected function _set_post_terms( $post_id, $term_id, $taxonomy ) {
		$this->_log->write( "Setting post terms for post ID: $post_id, term_id: $term_id, taxonomy: $taxonomy" );
		wp_set_post_terms( $post_id, array( $term_id ), $taxonomy );
	}

	/**
	 * Get the WordPress post ID, if the job already exists in the database
	 * @param  integer $unique_id The unique ID for the job.
	 * @return boolean|integer    The post ID if it exists, false otherwise
	 */			
	protected function _get_post_id( $unique_id ) {
		$posts = get_posts( array(
			'post_type' => 'job',
			'post_status' => 'publish',
			'posts_per_page' => 1,
			'meta_query' => array(
				array(
					'key' => 'unique_id',
					'value' => $unique_id,
					'compare' => '='
				)
			)
		) );
		
		if ( is_array( $posts ) && count( $posts ) > 0 ) {
			$post = reset( $posts );
			return $post->ID;
		}

		return false;
	}

	/**
	 * Check if the post should be updated based on the last modified date.
	 * @param  integer $post_id The post ID to check.
	 * @return boolean
	 */
	protected function _should_post_be_updated( $post_id ) {
		date_default_timezone_set('Europe/London');

		$xml_last_modified = (string)$this->_xml->lastmodified;
		$post_last_modified = get_post_meta( $post_id, 'last_modified', true );

		// Convert to DateTime objects
		$xml_last_modified = DateTime::createFromFormat( 'Ymd H:i:s', $xml_last_modified );
		$post_last_modified = DateTime::createFromFormat( 'Ymd H:i:s', $post_last_modified );

		return $xml_last_modified > $post_last_modified;
	}

	/**
	 * Check if a post has a specific term object attached.
	 * @param  integer $post_id The post ID to check.
	 * @param  Object $term     The WordPress term object.
	 * @param  string $term     The taxonomy name.
	 * @return boolean
	 */
	protected function _post_has_term( $post_id, $term, $taxonomy ) {
		// Get the sector terms on the post
		$post_sectors = get_the_terms( $post_id, $taxonomy );

		$post_has_term = false;
		if ( is_array( $post_sectors ) && count( $post_sectors ) ) {

			foreach ($post_sectors as $post_sector) {
				if ( $post_sector->term_id === $term->term_id ) {
					$post_has_term = true;
					break;
				}
			}

		}

		return $post_has_term;
	}

	/**
	 * Get the name of the log file currently being used.
	 * @return string
	 */
	public function get_log_file_name() {
		return $this->_log->datetime . '.log';
	} 
}
