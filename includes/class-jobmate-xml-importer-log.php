<?php

/**
 * Responsible for processing a single job and adding it to the WordPress database.
 *
 * @link       http://www.iamadamtaylor.com
 * @since      1.0.0
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 */

/**
 * Responsible for processing a single job and adding it to the WordPress database.
 *
 * Parses the maeta from the XML feed into usable values.
 * Creates job posts based on the parsed values
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 * @author     Adam Taylor <sayhi@iamadamtaylor.com>
 */
class JobMate_XML_Importer_Log {

	/**
   * @var Singleton The reference to *Singleton* instance of this class
   */
	private static $instance;

	/**
	 * The path to the logs directory
	 * @var string
	 */
	public $log_dir;

	/**
	 * Store the date & time when the logger was initialised.
	 * @var string
	 */
	public $datetime;

	/**
   * Returns the *Singleton* instance of this class.
   *
   * @return Singleton The *Singleton* instance.
   */
  public static function get_instance() {
    if (null === static::$instance) {
      static::$instance = new static();
    }
    
    return static::$instance;
  }

	protected function __construct() {
		date_default_timezone_set('Europe/London');

		// Store the path to the logs directory
		$this->log_dir = plugin_dir_path( __FILE__ ) . '../logs/';

		if ( !( is_dir( $this->log_dir ) && is_readable( $this->log_dir ) && is_writable( $this->log_dir ) ) ) {
			mkdir( $this->log_dir, 0777 );
		}

		// Store date and time when logger is initialised to write all further logs to the same file
		// Each request should initialise it's own logger for granular reporting
		$this->datetime = date('Ymd-H-i-s');
	}

	/**
   * Private clone method to prevent cloning of the instance of the
   * *Singleton* instance.
   *
   * @return void
   */
  private function __clone() {}

  /**
   * Private unserialize method to prevent unserializing of the *Singleton*
   * instance.
   *
   * @return void
   */
  private function __wakeup() {}
	
	/**
	 * Write a string to a .log file inside the plugin directory.
	 * @param  string $string
	 */
	public function write( $string, $blank_line_after = false ) {
		if ( !$this->_is_log_enabled() ) {
			return;
		}

		error_log( $string . PHP_EOL . ( $blank_line_after ? PHP_EOL : '' ), 3, $this->_get_log_filepath() );
	}

	/**
	 * Get the file path for this log instance.
	 * @return string
	 */
	protected function _get_log_filepath() {
		return $this->log_dir . $this->datetime . '.log';
	}

	/**
	 * Check whether logging is enabled.
	 * @return boolean
	 */
	private function _is_log_enabled() {
		return ( defined( 'WP_DEBUG' ) && WP_DEBUG ) && ( !defined( 'JOBMATE_IMPORT_DEBUG_LOG' ) || JOBMATE_IMPORT_DEBUG_LOG );
	}

}
