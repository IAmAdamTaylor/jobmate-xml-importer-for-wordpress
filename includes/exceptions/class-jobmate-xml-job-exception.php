<?php

/**
 * Custom Exception class for exceptions during job processing.
 *
 * @link       http://www.iamadamtaylor.com
 * @since      1.0.0
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/public
 */
class JobMate_XML_Job_Exception extends Exception {}
