<?php

/**
 * Creates and manages terms on taxonomies.
 *
 * @link       http://www.iamadamtaylor.com
 * @since      1.0.0
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 */

/**
 * Creates and manages terms on taxonomies.
 *
 * Creates new terms and as when required during the import process.
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 * @author     Adam Taylor <sayhi@iamadamtaylor.com>
 */
class JobMate_XML_Importer_Taxonomy_Manager {

	/**
	 * The name of the taxonomy that this instance refers to
	 * @var string
	 */
	protected $_taxonomy;

	/**
	 * Logger for writing debug statements
	 * @var JobMate_XML_Importer_Log
	 */
	protected $_log;

	function __construct( $taxonomy ) {
		$this->_taxonomy = $taxonomy;
		$this->_log = JobMate_XML_Importer_Log::get_instance();
	}

	/**
	 * Gets a taxonomy term from the database. If it doesn't exist the term is created
	 * @param  string $name The name of the term 
	 * @return Object 		  A WordPress term object.
	 */
	public function get_term( $name ) {
		$term = $this->_get_term_by_name( $name );

		// Term was found
		if ( false !== $term ) {
			$this->_log->write( "Term: $name for taxonomy: {$this->_taxonomy} was found, term_id: {$term->term_id}, slug: {$term->slug}" );
			return $term;
		}

		// Else, create term
		$this->_log->write( "Term: $name for taxonomy: {$this->_taxonomy} was not found, creating" );

		return $this->_create_term( $name );
	}

	/**
	 * Get a term by it's name
	 * @param  string $name The name of the term.
	 * @return Object     	A WordPress term object.  
	 */
	protected function _get_term_by_name( $name ) {
		$slug = sanitize_title( $name );
		return get_term_by( 'slug', $slug, $this->_taxonomy );
	}

	/**
	 * Create a new term.
	 * @param  string $name The name of the term.
	 * @return Object 			A WordPress term object.
	 */
	protected function _create_term( $name ) {
	
		$term_meta = wp_insert_term( $name, $this->_taxonomy );

		if ( is_wp_error( $term_meta ) ) {
			$exception_message = "Term: $name for taxonomy: {$this->_taxonomy} could not be created";
			$this->_log->write( "Thrown Exception: " . $exception_message );
			throw new JobMate_XML_Job_Exception( $exception_message );
		}

		$term = get_term_by( 'id', $term_meta['term_id'], $this->_taxonomy );
		$this->_log->write( "Created term: $name for taxonomy: {$this->_taxonomy}, term_id: {$term->term_id}, slug: {$term->slug}" );

		return $term;
	}

}
