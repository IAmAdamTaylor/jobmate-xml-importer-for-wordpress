<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.iamadamtaylor.com
 * @since      1.0.0
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 * @author     Adam Taylor <sayhi@iamadamtaylor.com>
 */
class JobMate_XML_Importer {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      JobMate_XML_Importer_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'jobmate-xml-importer';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - JobMate_XML_Importer_Loader. Orchestrates the hooks of the plugin.
	 * - JobMate_XML_Importer_i18n. Defines internationalization functionality.
	 * - JobMate_XML_Importer_Admin. Defines all hooks for the admin area.
	 * - JobMate_XML_Importer_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class for logging debug messages.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-jobmate-xml-importer-log.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-jobmate-xml-importer-loader.php';

		/**
		 * The class for managing taxonomy terms.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-jobmate-xml-importer-taxonomy-manager.php';

		/**
		 * The class for parsing & importing the XML file.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-jobmate-xml-job.php';

		/**
		 * The class for parsing & importing the XML file.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/exceptions/class-jobmate-xml-job-exception.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-jobmate-xml-importer-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-jobmate-xml-importer-public.php';

		$this->loader = new JobMate_XML_Importer_Loader();

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new JobMate_XML_Importer_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_admin, 'register_request_url' );
		$this->loader->add_filter( 'query_vars', $plugin_admin, 'register_query_vars' );
		$this->loader->add_action( 'parse_request', $plugin_admin, 'parse_request' );

		$this->loader->add_action( 'jbm_handle_import_request', $plugin_admin, 'handle_import_request', 10, 1 );

	}

	/**
	 * Register all of the hooks related to the user-facing functionality
	 * of the plugin.
	 * Users are designed to be the WordPress admins in this case, so register post types etc. here
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new JobMate_XML_Importer_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $plugin_public, 'register_job_post_type' );
		$this->loader->add_action( 'init', $plugin_public, 'register_sector_taxonomy' );
		$this->loader->add_action( 'manage_edit-job_columns', $plugin_public, 'add_admin_columns' );
		$this->loader->add_action( 'manage_edit-job_sortable_columns', $plugin_public, 'add_sortable_admin_columns' );
		$this->loader->add_action( 'manage_job_posts_custom_column', $plugin_public, 'show_data_for_admin_columns', 10, 2 );
		$this->loader->add_action( 'pre_get_posts', $plugin_public, 'sort_admin_columns' );
		$this->loader->add_action( 'pre_get_posts', $plugin_public, 'search_by_meta' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    JobMate_XML_Importer_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
