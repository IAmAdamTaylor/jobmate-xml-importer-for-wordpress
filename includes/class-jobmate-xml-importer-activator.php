<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.iamadamtaylor.com
 * @since      1.0.0
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/includes
 * @author     Adam Taylor <sayhi@iamadamtaylor.com>
 */
class JobMate_XML_Importer_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {		
		$plugin_name = 'jobmate-xml-importer';
		$version = '1.0.0';

		$plugin_admin = new JobMate_XML_Importer_Admin( $plugin_name, $version );
		$plugin_public = new JobMate_XML_Importer_Public( $plugin_name, $version );

		// Call all functions that change the permalink structure
		$plugin_public->register_job_post_type();
		$plugin_admin->register_request_url();

		// Flush the rewrite rules to include our new virtual path
		flush_rewrite_rules();
	}

}
