<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.iamadamtaylor.com
 * @since      1.0.0
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/admin
 * @author     Adam Taylor <sayhi@iamadamtaylor.com>
 */
class JobMate_XML_Importer_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Import a job from a POST request.
	 * @param  string $request_body The POST request body, a string of well formed XML.
	 */
	public function handle_import_request( $request_body ) {
		// Convert request to XML
		$xml = new SimpleXMLElement( $request_body );

		if ( !$xml ) {
			return;
		}

		// Import the job
		// An exception will be thrown if the import fails for whatever reason
		$exception_thrown = false;
		try {
			
			$job = new JobMate_XML_Job( $xml );
			
		} catch (JobMate_XML_Job_Exception $e) {
			
			$exception_thrown = true;
			echo "Exception caught: {$e->getMessage()}";

		}

		if ( !$exception_thrown ) {
			
			echo "Request has been processed with no fatal errors.<br>";

			switch ( $job->status ) {
				case 'processing':
					$log_file_name = $job->get_log_file_name();
					echo "Request status is incomplete. Part or all of the request may not have imported correctly.<br>";
					echo "Check the log file $log_file_name for more details.";
					break;

				case 'processed':
					echo "Request finished processing. No updates were made.";
					break;
				
				default:
					echo "Job was {$job->status} successfully.";
					break;
			}

		}

	}

	/**
	 * Register a permalink to point to our run.php file at the plugin root.
	 * Uses a query variable to green flag to parse_request that this request should be intercepted.
	 */
	public function register_request_url() {
		if ( _jobmate_xml_importer_is_deactivating() ) {
			return;
		}

		if ( $this->_is_test_mode() ) {
			// Create a URL for sending test requests
			add_rewrite_rule('^jobmate-import\/test-request\/?', 'index.php?jobmatexmlimport_test=1', 'top');
		}

		// Create a URL for receiving the requests (test & production)
		add_rewrite_rule('^jobmate-import\/run\/?', 'index.php?jobmatexmlimport=1', 'top');
	}

	public function register_query_vars( $query_vars ) {
		if ( _jobmate_xml_importer_is_deactivating() ) {
			return;
		}

		if ( $this->_is_test_mode() ) {
			$query_vars[] = 'jobmatexmlimport_test';
		}

		$query_vars[] = 'jobmatexmlimport';

		return $query_vars;
	}

	public function parse_request( &$wp ) {
		if ( _jobmate_xml_importer_is_deactivating() ) {
			return;
		}
		
    if ( $this->_is_test_mode() && array_key_exists( 'jobmatexmlimport_test', $wp->query_vars ) ) {
			require_once plugin_dir_path( __FILE__ ) . '../test-request.php';
      exit();
    }

		if ( array_key_exists( 'jobmatexmlimport', $wp->query_vars ) ) {
			require_once plugin_dir_path( __FILE__ ) . '../run.php';
      exit();
    }
    
    return;
	}

	private function _is_test_mode() {
		return defined( 'JOBMATE_IMPORT_TEST_MODE' ) && JOBMATE_IMPORT_TEST_MODE;
	}

}
