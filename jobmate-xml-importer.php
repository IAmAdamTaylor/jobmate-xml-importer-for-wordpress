<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.iamadamtaylor.com
 * @since             1.0.0
 * @package           JobMate_XML_Importer
 *
 * @wordpress-plugin
 * Plugin Name:       JobMate XML Importer
 * Plugin URI:        http://www.iamadamtaylor.com/resource/jobmate-xml-importer/
 * Description:       Import job postings from the Recruitive JobMate software. Creates a 'job' post type and then handles receiving individual POST requests for each job. The URL for these requests can be configured inside the plugin.
 * Version:           1.0.0
 * Author:            I Am Adam Taylor
 * Author URI:        http://www.iamadamtaylor.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       jobmate-xml-importer
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-jobmate-xml-importer-activator.php
 */
function activate_jobmate_xml_importer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-jobmate-xml-importer-activator.php';
	require_once plugin_dir_path( __FILE__ ) . 'admin/class-jobmate-xml-importer-admin.php';
	require_once plugin_dir_path( __FILE__ ) . 'public/class-jobmate-xml-importer-public.php';
	JobMate_XML_Importer_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-jobmate-xml-importer-deactivator.php
 */
function deactivate_jobmate_xml_importer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-jobmate-xml-importer-deactivator.php';
	JobMate_XML_Importer_Deactivator::deactivate();
}

function _jobmate_xml_importer_is_deactivating() {
	return 
		is_admin() &&
		isset( $_GET['action'], $_GET['plugin'] ) && 
		'deactivate' === $_GET['action'] &&
		plugin_basename( __FILE__ ) === $_GET['plugin'];
}

register_activation_hook( __FILE__, 'activate_jobmate_xml_importer' );
register_deactivation_hook( __FILE__, 'deactivate_jobmate_xml_importer' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-jobmate-xml-importer.php';

/**
 * API wrapper to allow easy access to the job data.
 * Included here to allow WordPress templates access to the class.
 */
require_once plugin_dir_path( __FILE__ ) . 'public/class-jobmate-job.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_jobmate_xml_importer() {

	$plugin = new JobMate_XML_Importer();
	$plugin->run();

}
run_jobmate_xml_importer();
