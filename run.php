<?php

// <!-- Function Defs -->

  /**
   * Check if the request's server IP is in our safe sender list.
   * @param string $ip The IP of the server making the request.
   * @return boolean 
   */
  function is_safe_ip( $ip ) {
    // If we are in test mode accept any requests
    if ( defined( 'JOBMATE_ACCEPT_ALL_REQUESTS' ) && JOBMATE_ACCEPT_ALL_REQUESTS ) {
      return true;
    }

    // Declare IPs that we will accept requests from
    $safe_ips = array(
      '195.62.28.68', // Recruitive JobMate posting service
      '82.153.14.64', // Recruitive JobMate manual send (if service fails)
    );

    return in_array( $ip, $safe_ips );
  }

  /**
   * Check if the connecting IP is safe.
   * A secondary abstracted function because the IP can be coming from 2 different places.
   * @return boolean 
   */
  function check_ip() {
    $ip = null;

    if ( isset( $_SERVER['HTTP_CF_CONNECTING_IP'] ) ) {
      $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
    } else if ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
      $ip = $_SERVER['REMOTE_ADDR'];
    }

    if ( !isset( $ip ) ) {
      return false;
    }

    return is_safe_ip( $ip );
  }

  /**
   * Send the appropriate response headers.
   * @param  integer $status_code A HTTP status code.
   */
  function send_response_headers( $status_code ) {
    // Use WordPress function to quickly set status headers
    status_header( $status_code );
  }

  /**
   * Display the errors thrown when parsing an XML document.
   */
  function display_xml_error($error, $xml) {
    $return  = htmlspecialchars( $xml[$error->line - 1] ) . "\n";
    $return .= str_repeat('-', $error->column) . "^\n";

    switch ($error->level) {
      case LIBXML_ERR_WARNING:
        $return .= "Warning $error->code: ";
        break;
       case LIBXML_ERR_ERROR:
        $return .= "Error $error->code: ";
        break;
      case LIBXML_ERR_FATAL:
        $return .= "Fatal Error $error->code: ";
        break;
    }

    $return .= trim($error->message) .
               "\n  Line: $error->line" .
               "\n  Column: $error->column";

    if ($error->file) {
      $return .= "\n  File: $error->file";
    }

    return "$return\n\n--------------------------------------------\n\n";
  }

  if (!function_exists('get_all_headers')) {
    function get_all_headers() {
      $headers = ''; 
      foreach ($_SERVER as $name => $value) {
        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', $name))))] = $value; 
      } 
      return $headers; 
    } 
  } 

// </!-- Function Defs -->

// Due to Cross Origin header need to specify that because of Origin response may vary.
header( "Vary: Accept-Encoding, Origin" );

// Set Cross Origin access, allowing JobMate poster
header( "Access-Control-Allow-Origin: *" );
header( "Access-Control-Allow-Headers: X-PINGOTHER, Content-Type" );

// Check that remote IP is accessible, if not reject the request
// Cannot send any response data or unauthorised headers back: 
//    Could be a preflight request which will fail if we send bad headers
//    Exposes interesting things to an attacker if we do, e.g. We have a safe IP range set, safer just to fail
if ( !check_ip() ) {
  exit();
}

// Get log instance
$log = JobMate_XML_Importer_Log::get_instance();
$log->write( 'Request started.', true );
$log->write( 'HTTP Request headers are:' );
$log->write( "\t" . json_encode( get_all_headers() ), true );


// Reject anything other than a POST or OPTIONS (Preflighted) request
if ( 'POST' !== $_SERVER['REQUEST_METHOD'] && 'OPTIONS' !== $_SERVER['REQUEST_METHOD'] ) {
  send_response_headers( 405 );
  $message = $_SERVER["REQUEST_METHOD"] . ' is not accepted.';
  $log->write( $message );
  echo $message;

  exit();
}

// Read the XML request
$request_body = file_get_contents('php://input');

// If blank body, simply exit. Could be a preflight response which will fail if we send bad headers
if ( '' === $request_body ) {
  exit();
}

// Check the body is valid XML
libxml_use_internal_errors(true);
$test = simplexml_load_string( $request_body );
$test_lines = explode( "\n", $request_body );

if ( false === $test ) {
	$errors = libxml_get_errors();

	// If not, send bad request headers, debug information, then exit
	// 400 Bad Request
  send_response_headers(400);

  foreach ($errors as $error) {
  	$message = display_xml_error($error, $test_lines);
    echo nl2br( $message );
    $log->write( $message );
  }

  libxml_clear_errors();
	
	exit();
}

// Otherwise, send the appropriate response headers
// 200 OK
send_response_headers(200);

$message = 'Request is well formed.';
echo $message . '<br>';
$log->write( $message, true );

// And pass off to WordPress to handle
do_action( 'jbm_handle_import_request', $request_body );
exit();
