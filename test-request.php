<?php

function get_origin() {
    $url = parse_url( home_url() );
    $origin = '';

    if ( isset( $url['scheme'] ) ) {
        $origin .= $url['scheme'] . '://';
    } else {
        $origin .= 'http://';
    }

    $origin .= $url['host'];

    if ( isset( $url['port'] ) ) {
        $origin .= ':' . $url['port'];
    }

    return $origin;
}

// Create a request to the URL
$url = home_url( '/jobmate-import/run/' );
$ch = curl_init( $url );

echo "Starting test request to $url<br><br>";

// We want to send this XML
// Generate a random integer to use as the Unique ID
$rand = rand(1, 2147483640 - 100);
$request_body = '<?xml version="1.0" encoding="utf-8"?>
<job>
    <uniqueid>19219291</uniqueid>
    <lastmodified>20161006 16:43:45</lastmodified>
    <jobreference>DHCBoct</jobreference>
    <jobtitle>HGV C+E Class 2 Drivers</jobtitle>
    <hours>Full Time</hours>
    <jobtype>Permanent</jobtype>
    <jobduration></jobduration>
    <location>
        <name>Hounslow</name>
        <latitude>51.4713012</latitude>
        <longitude>-0.48782649999998284</longitude>
    </location>
    <sector>Transport &amp; Logistics</sector>
    <branchname>H&amp;G Essex</branchname>
    <companyname></companyname>
    <immediatestart>true</immediatestart>
    <daterange>
        <startdate>20161006</startdate>
        <enddate>20161105</enddate>
    </daterange>
    <salary>
        <min>9</min>
        <max>17</max>
        <type>Per Hour</type>
    </salary>
    <jobdescription><![CDATA[<p>Job Description</p><p></p><p>H&amp;G Recruitment requires HGV Class 2 ( C category) drivers to work at Heathrow Airport. This work is on a roster basis so some weekend work is required. Rosters are 5 on 3 off. Start times vary, so you are required to be flexible. Temp to perm opportunities available. H&amp;G Recruitment would need to apply for a full airside pass for you to gain any placement within the airside industry. Five years employment reference and address history is required to complete the DBS check<br><br>Requirements: Class 2 licence (Category C) - HGV Licence must be held for more than 2 years. You must be able to pass a full driving assessment this is carried out at a driving test standard.<br><br>Full training is issued by the client on the commencement of your placement. The work entails the loading and unloading of an aircraft. You will service 2 to 3 flights per day, all drivers are assisted by a loader.<br><br><br><br>We offer:<br><br>* Full time HGV driving work with H&amp;G Recruitment<br><br>* Maximised earning potential on £9 - £17 per hour<br><br>* Plenty of overtime<br><br>* Flexible shifts to suit you<br><br>* Estimated earnings £650+ per week<br><br><br><br>Benefits:<br><br>* Job security working for a diverse blue chip client<br><br>* Genuine reward schemes<br><br>* Free DCPC Courses (conditions apply)<br><br>* Recommend a friend bonus (T&amp;C`s)<br><br>* Free uniform<br><br><br><br>You must have:<br><br>* Full HGV C+E Class 2 licence held for at least two years<br><br>* Driven a HGV lorry in the last 90 days<br><br>* Five years consistent working history that can be referenced (no Gaps allowed)<br><br>* Clean criminal record to obtain full airside pass<br><br>* Digital tacho card<br><br>* A good understanding of Drivers hours and regulations<br><br>* Good understanding of the working time directive<br><br>* Excellent geographical knowledge of UK road network and the London Lorry Scheme<br><br>* Flexible approach to work<br><br>* Customer focused attitude<br><br>* Superb presentation standards<br><br>* A willingness to learn<br><br><br><br>If this sounds like the job for you don`t delay in applying. Call 0845 561 6000 or email your interest &amp; details/CV to enquiries@h-g-recruitment.com<br><br>H&amp;G Recruitment are one of the country`s fastest growing and leading logistic and HGV C+E Class 1 driver recruitment experts in the supply chain, we have large key account contracts with some of the largest blue chip distribution companies, national hauliers and supermarket chains in the country. Since 2003 H&amp;G Recruitment have offered a caring and considerate service to their workforce; come and be part of our winning team - so why delay APPLY today<br><br><br>&nbsp;</p><p></p>]]></jobdescription>
    <jobsummary><![CDATA[Job Description<br/><br/><br/><br/>H&G Recruitment requires HGV Class 2 ( C category) drivers to work at Heathrow Airport. This work is on a roster basis so some weekend work is required. Rosters are ]]></jobsummary>
    <keyfeatures>
        <keyfeature></keyfeature>
    </keyfeatures>
    <benefits>
        <benefit></benefit>
    </benefits>
    <jobrequirements>
        <jobrequirement></jobrequirement>
    </jobrequirements>
    <application_email>19219291@jobs.jobmate.biz</application_email>
    <URL></URL>
</job>';

// Set up options
curl_setopt_array($ch, array(
  CURLOPT_POST => TRUE,
  CURLOPT_RETURNTRANSFER => TRUE,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: text/xml',
    'Origin: ' . get_origin()
  ),
  CURLOPT_POSTFIELDS => $request_body
));

// Send the request
$response = curl_exec($ch);

// Check for errors
if($response === FALSE){
  die( curl_error($ch) );
}

// Print the response
echo $response;

if ( '' === $response ) {
    echo "No response. Request may have been rejected due to IP addresses. Check safe sender list of run.php";
}

exit();
