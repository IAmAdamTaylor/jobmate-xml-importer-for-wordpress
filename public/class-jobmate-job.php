<?php

/**
 * API wrapper to allow easy access to the job data.
 *
 * @link       http://www.iamadamtaylor.com
 * @since      1.0.0
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/public
 * @author     Adam Taylor <sayhi@iamadamtaylor.com>
 */
class JobMate_Job {

	/**
	 * The post ID this job refers to.
	 * @var integer
	 */
	public $ID;

	/**
	 * An associative array that caches any values returned through __get or __isset.
	 * These properties will attempt to fetch a value from the database, 
	 * which could be costly if used multiple times for the same property.
	 * @var array
	 */
	protected $_cache = array();

	function __construct( $post ) {
		if ( is_a( $post, 'WP_Post' ) ) {
			$post = $post->ID;
		}

		$this->ID = $post;
	}

	/**
	 * Overload __get magic method, to return properties from the database if not found.
	 * @param  string $key The property attempted to access.
	 * @return mixed 			 The value from the database, or the cache if already stored.
	 */
	function __get( $key ) {
		// Check if in the cache

		// If not we can get it from the database, and cache after
		$value = $this->_get_meta( $key );

		// Convert an array property to an object if it has keys which are strings
		if ( is_array( $value ) && $this->_array_has_string_keys( $value ) ) {
			$value = (object)$value;
		}

		if ( 'salary' == $key ) {
			$value->min_formatted = '&pound;' . number_format( $value->min, 0 );
			$value->max_formatted = '&pound;' . number_format( $value->max, 0 );
			$value->type = strtolower( $value->type );
		}

		// Convert booleans
		if ( 'true' === $value ) {
			$value = true;
		}
		if ( 'false' === $value ) {
			$value = false;
		}

		return $value;
	}

	function __isset( $key ) {
		return ( $this->_get_meta( $key ) === true );
	}

	/**
	 * Store a value in the in-object cache.
	 * @param  string $key   The property requested.
	 * @param  mixed $value  The value retrieved from the database.
	 */
	public function store_cache_item( $key, $value ) {
		$this->_cache[ $key ] = $value;
	}

	/**
	 * Clears the in-object cache forcing all values to be retrieved from the database.
	 */
	public function clear_cache() {
		$this->_cache = array();
	}

	/**
	 * Removes a single property from the in-object cache.
	 * @param $key The property name to remove.
	 */
	public function clear_cache_item( $key ) {
		if ( isset( $this->_cache[ $key ] ) ) {
			unset( $this->_cache[ $key ] );
		}
	}

	/**
	 * Update the cache for a single property and return the new value.
	 * @param  string $key The property to update.
	 * @return mixed       The latest value from the database.
	 */
	public function update_cache_item( $key ) {
		$this->clear_cache_item( $key );
		return $this->_get_meta( $key );
	}

	/**
	 * Get the post meta.
	 * @param  string $key 		The meta key to get.
	 * @param  boolean $cache Optional, true if the returned value should be cached, false if not.
	 * @return mixed
	 */
	protected function _get_meta( $key, $cache = true ) {
		$value = get_post_meta( $this->ID, $key, true ); 
		
		if ( $cache ) {
			$this->store_cache_item( $key, $value );
		}

		return $value;
	}

	/**
	 * Check if an array has any string keys.
	 * @param  array $array
	 * @return boolean
	 */
	protected function _array_has_string_keys( $array ) {
		return count( array_filter( array_keys( $array ), 'is_string') ) > 0;
	}
}
