<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.iamadamtaylor.com
 * @since      1.0.0
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    JobMate_XML_Importer
 * @subpackage JobMate_XML_Importer/public
 * @author     Adam Taylor <sayhi@iamadamtaylor.com>
 */
class JobMate_XML_Importer_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	const JOBMATE_POST_TYPE = 'job';
	const JOBMATE_POST_TYPE_PLURAL = 'Jobs';
	const JOBMATE_POST_TYPE_SINGULAR = 'Job';

	const JOBMATE_SECTOR_TAXONOMY = 'sector';
	const JOBMATE_SECTOR_TAXONOMY_PLURAL = 'Sectors';
	const JOBMATE_SECTOR_TAXONOMY_SINGULAR = 'Sector';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function register_job_post_type() {
		if ( _jobmate_xml_importer_is_deactivating() ) {
			return;
		}
		
		$labels = array(
			'name'               => _x( self::JOBMATE_POST_TYPE_PLURAL, 'post type general name' ),
			'singular_name'      => _x( 'Job', 'post type singular name' ),
			'menu_name'          => _x( self::JOBMATE_POST_TYPE_PLURAL, 'admin menu' ),
			'name_admin_bar'     => _x( self::JOBMATE_POST_TYPE_SINGULAR, 'add new on admin bar' ),
			'add_new'            => _x( 'Add New', self::JOBMATE_POST_TYPE ),
			'add_new_item'       => __( 'Add New ' . self::JOBMATE_POST_TYPE_SINGULAR ),
			'new_item'           => __( 'New ' . self::JOBMATE_POST_TYPE_SINGULAR ),
			'edit_item'          => __( 'Edit ' . self::JOBMATE_POST_TYPE_SINGULAR ),
			'view_item'          => __( 'View ' . self::JOBMATE_POST_TYPE_SINGULAR ),
			'all_items'          => __( 'All ' . self::JOBMATE_POST_TYPE_PLURAL ),
			'search_items'       => __( 'Search ' . self::JOBMATE_POST_TYPE_PLURAL ),
			'parent_item_colon'  => __( 'Parent ' . self::JOBMATE_POST_TYPE_PLURAL . ':' ),
			'not_found'          => __( 'No '. strtolower( self::JOBMATE_POST_TYPE_PLURAL ) .' found.' ),
			'not_found_in_trash' => __( 'No '. strtolower( self::JOBMATE_POST_TYPE_PLURAL ) .' found in Trash.' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => self::JOBMATE_POST_TYPE, 'with_front' => false ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'menu_icon'			 		 =>	'dashicons-businessman',
			'supports'       		 => array( 'title', 'editor', 'excerpt', 'custom-fields' ),
		);

		register_post_type( self::JOBMATE_POST_TYPE, $args );
	}

	public function register_sector_taxonomy() {
		if ( _jobmate_xml_importer_is_deactivating() ) {
			return;
		}
		
		$labels = array(
			'name'              => _x( self::JOBMATE_SECTOR_TAXONOMY_PLURAL, 'taxonomy general name' ),
			'singular_name'     => _x( self::JOBMATE_SECTOR_TAXONOMY_SINGULAR, 'taxonomy singular name' ),
			'search_items'      => __( 'Search ' . self::JOBMATE_SECTOR_TAXONOMY_PLURAL ),
			'all_items'         => __( 'All ' . self::JOBMATE_SECTOR_TAXONOMY_PLURAL ),
			'parent_item'       => __( 'Parent ' . self::JOBMATE_SECTOR_TAXONOMY_SINGULAR ),
			'parent_item_colon' => __( 'Parent ' . self::JOBMATE_SECTOR_TAXONOMY_SINGULAR . ':' ),
			'edit_item'         => __( 'Edit ' . self::JOBMATE_SECTOR_TAXONOMY_SINGULAR ),
			'update_item'       => __( 'Update ' . self::JOBMATE_SECTOR_TAXONOMY_SINGULAR ),
			'add_new_item'      => __( 'Add New ' . self::JOBMATE_SECTOR_TAXONOMY_SINGULAR ),
			'new_item_name'     => __( 'New ' . self::JOBMATE_SECTOR_TAXONOMY_SINGULAR . ' Name' ),
			'menu_name'         => __( self::JOBMATE_SECTOR_TAXONOMY_PLURAL ),
			'not_found'					=> __('No ' . strtolower( self::JOBMATE_SECTOR_TAXONOMY_PLURAL ) . ' found'),
		);
		
		$args = array(  
	    'hierarchical' => true,  
	    'labels' => $labels,
	    'show_admin_column' => true,
	    'query_var' => true,
	    'rewrite' => array(
	      'slug' => self::JOBMATE_SECTOR_TAXONOMY, // Rewrite the permalink displayed if needed
	      'with_front' => false, 
	    )
		);
		
	  register_taxonomy( self::JOBMATE_SECTOR_TAXONOMY, self::JOBMATE_POST_TYPE, $args );
	}

	public function add_admin_columns( $columns ) {
		$new_columns = array();

	  foreach ($columns as $key => $value) {
	    $new_columns[ $key ] = $value;
	    if ( $key == 'title' ) {
	      $new_columns['jbm_unique_id'] = "Unique ID";
	      $new_columns['jbm_reference'] = "Job Reference";
	      $new_columns['jbm_closing_date'] = "Closing Date";
	    }
	  }

	  return $new_columns;
	}

	public function add_sortable_admin_columns( $columns ) {
		$columns['jbm_unique_id'] = "job_unique_id";
    $columns['jbm_reference'] = "job_reference";
    $columns['jbm_closing_date'] = "closing_date";
	  return $columns;
	}

	public function show_data_for_admin_columns( $column_name, $post_id ) {
		global $post;

		if ( 'jbm_closing_date' === $column_name ) {
			date_default_timezone_set('Europe/London');

			$date = get_post_meta( $post_id, 'end_date', true );
			$date = DateTime::createFromFormat( 'Ymd', $date );

			$today = new DateTime();
			$has_ended = ( $today > $date );

			echo sprintf( 
				'%s<br><strong style="%s">%s</strong>', 
				$date->format( 'd/m/Y' ),
				( $has_ended ? 'color: #dc3232;' : '' ),
				( $has_ended ? 'Ended' : 'Active' )
			);
			return;
		}

		$column_map = array(
			'jbm_unique_id' => 'unique_id',
			'jbm_reference' => 'job_reference'
		);

		echo get_post_meta( $post_id, $column_map[ $column_name ], true );
	}

	public function search_by_meta( $query ) {
		// Extract the search terms
    $search_term = $query->query_vars['s'];

	  if ( is_admin() && '' !== $search_term ) {

			// Enable search on these meta keys
			$meta_keys = array(
	      'unique_id',
	      'job_reference'
	    );

	    // We have to unset the search term otherwise it will still stop posts from being shown
	    $query->query_vars['s'] = '';

	    // Create a new meta query
	    $meta_query = array();

	    if ( '' !== $search_term ) {
	      foreach ($meta_keys as $meta_key) {
	      	$meta_query[] = array(
	      		'key' => $meta_key,
	      		'value' => $search_term,
	      		'compare' => 'LIKE'
	      	);
	      }

	      if ( count( $meta_query ) > 1 ) {
		      $meta_query['relation'] = 'OR';
	      }

	      $query->set( 'meta_query', $meta_query );
	    }

		}

	}

	public function sort_admin_columns( $query ) {
		if ( 
			!is_admin() || 
			self::JOBMATE_POST_TYPE !== $query->get('post_type') ||
			!( $orderby = $query->get('orderby') )
		) {
			return;
		}

		// Check for sort keys in the query vars and convert to meta query
		if ( 'job_unique_id' === $orderby ) {
			$query->set( 'meta_key', 'unique_id' );
			$query->set( 'orderby', 'meta_value_num' );
		}

		if ( 'job_reference' === $orderby ) {
			$query->set( 'meta_key', 'job_reference' );
			$query->set( 'orderby', 'meta_value' );
		}
	}

}
