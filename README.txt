=== JobMate XML Importer ===
Donate link: http://www.iamadamtaylor.com/
Tags: jobmate, xml, import, jobs, job, recruitment
Requires at least: 4.6
Tested up to: 4.6
Stable tag: 4.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Import job postings from the Recruitive JobMate software. Creates a 'job' post type and then handles receiving individual POST requests for each job. The URL for these requests can be configured inside the plugin.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `jobmate-xml-importer.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

The plugin registers a permalink at SITE_URL/jobmate-import/run/ which can accept POST requests with XML as the body of the request.

To show the jobs on the front end, some devleopment work will be required. This plugin does not provide shortcodes to output job listings and single pages.

= Testing the Request Permalink =

**Make sure these settings are removed, or set to false, on production servers.**

To test the requests, you can define two values in your wp-config.php file. Add these 2 lines to your wp-config.php file:

define( 'JOBMATE_IMPORT_TEST_MODE', true );
define( 'JOBMATE_ACCEPT_ALL_REQUESTS', true );

If you've already activated the plugin then you will also need to refresh the WordPress permalinks after adding this line. You can do this by going to Settings > Permalinks in the WordPress admin and clicking Save Changes. **N.B. You don't need to change the permalink setting. Clicking Save Changes alone is enough.**

The first define line enables a testing URL on your website. You can go to this URL in your browser to send a test request through to the plugin. The URL is: YOUR_SITE_URL/jobmate-import/test-request/

The second line makes the import script bypass some of it's security checks, so that you can be sure your test will be accepted.

**Again, make sure these settings are removed, or set to false, on production servers.**

= Debug & Logging Settings =

If WP_DEBUG is enabled, .log files will be generated for each request in the logs folder inside the plugin. The .log files are named by the date and time the request occurred.

We advise you to enable this setting during, at least, the first week of using the plugin, to allow you to easily diagnose any potential issues you may have.

See https://codex.wordpress.org/WP_DEBUG for instructions on how to enable this.

= Stop .log files from being generated =

If you want to have WP_DEBUG enabled for the rest of your site and not generate .log files for this plugin, you can add a setting to your wp-config.php to stop these files being created.

Add the line below to your wp-config.php file:

define( 'JOBMATE_IMPORT_DEBUG_LOG', false );

== Frequently Asked Questions ==

= Why do I get a 404 error when I try to access the SITE_URL/jobmate-import/run/ URL? =

This plugin refreshes the WordPress permalink structure when activated, however, in rare cases you may have to refresh them manually.
You can do this by going to Settings > Permalinks in the WordPress admin and clicking Save Changes. **N.B. You don't need to change the permalink setting. Clicking Save Changes alone is enough.**

= Why do I get a 404 error when I try to access the SITE_URL/jobmate-import/test-request/ URL? = 

Firstly, make sure that you have defined the test mode value in your wp-config.php file. See the 'Testing the Request Permalink' section on the 'Installation' tab for more information on enabling this feature.

If you didn't have this feature enabled when you activated the plugin, you will need to refresh the WordPress permalink structure. You can do this by going to Settings > Permalinks in the WordPress admin and clicking Save Changes. **N.B. You don't need to change the permalink setting. Clicking Save Changes alone is enough.**

= Where are the .log files stored for this plugin? =

They are stored in the logs folder within the plugin folder. From the WordPress root folder the path is wp-content/plugins/jobmate-xml-importer/logs/

= I want to enable WP_DEBUG on my site, but don't want to generate lots of .log files for this plugin. Is this possible? =

Absolutely. You can add a value to your wp-config.php file to stop all .log files being generated. See the 'Debug & Logging Settings' section on the 'Installation' tab for more information on using this feature.
